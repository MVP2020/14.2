﻿using System;
using System.Windows.Forms;

namespace project2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            //Declare Array and Random
            Random mijnRandom = new Random();
            mijnRandom.Next(0, 101);
            int total = 0;
            int[,] myarray = new int[5, 3];

            //Define Loop to fill the array
            for (int i = 0; i < 5; i++)
            {
                myarray[i, 0] = mijnRandom.Next(0, 101); //i = row and 0,1,2 = columns 
                myarray[i, 1] = mijnRandom.Next(0, 101);
                myarray[i, 2] = myarray[i, 0] + myarray[i, 1];
                total = myarray[i, 2] + total; //Sum of 3rd column
            }
            //declare smallest number
            //declare largest number
            int smallestNumber = 101; //it will keep searching until its finds the smallest number.
            int biggestNumber = 0; // it will keep searching until it finds the largest number.
            foreach (int index in myarray)
            {
                if (index < smallestNumber)
                {
                    smallestNumber = index;
                }
                if (index > biggestNumber)
                {
                    biggestNumber = index;
                }
            }
            printArray(myarray);
            txtResults.Text = ("De som van al deze getallen is (3rd column): " + total + Environment.NewLine);
            for (int iRow = 0; iRow < 5; iRow++)
            {
                for (int iColumn = 0; iColumn < 3; iColumn++)
                {
                    if (smallestNumber == myarray[iRow, iColumn])
                    {
                        txtResults.Text = txtResults.Text + "Het kleinste getal is: " + smallestNumber + " Staat op rij " + iRow + " in kolom " + iColumn + Environment.NewLine;
                    }
                    if (biggestNumber == myarray[iRow, iColumn])
                    {
                        txtResults.Text = txtResults.Text + "Het grootste getal is: " + biggestNumber + " Staat op rij " + iRow + " in kolom " + iColumn + Environment.NewLine;
                    }
                }
            }
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void printArray(int[,] tePrintenArray)
        {
            textBox1.Text = "";
            for (int iRij = 0; iRij < tePrintenArray.GetUpperBound(0) + 1; iRij++)
            {
                for (int iKolom = 0; iKolom < tePrintenArray.GetUpperBound(1) + 1; iKolom++)
                {
                    textBox1.Text = textBox1.Text + tePrintenArray[iRij, iKolom].ToString() + "\t";
                }
                textBox1.Text = textBox1.Text + Environment.NewLine + Environment.NewLine;
            }
        }

    }
}
